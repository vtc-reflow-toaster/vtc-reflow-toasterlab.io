Conversion Kit\*
================
\* Kit not actually for sale; this is just a hacky school project

To make it easier to get started, a kit is available containing all the components required to convert an ordinary convection oven into a serviceable reflow oven.

All off-the-self components are independently available, and plans are available for all custom parts.

Oven Recommendations
--------------------
The kit has been tested with a single oven, but should work adequately with most ovens.  The BLACK+DECKER TO1675B convection oven was used during the development of the kit; materials and instructions are geared towards this oven.

Other ovens may be used instead.  A few characteristics are desirable:
 - Ample power: especially in large ovens, a large amount of power is required to rapidly heat the oven.  At least 1 kW per cubic foot is a reasonable target, but few ovens are capable of supplying this much heat.
 - An _effective_ circulation fan: forced air encourages heat to be transferred through convection rather than radiation.  This is desirable for soldering to provide uniform heating and to reduce the reliance on infrared for heat transfer.

Kit Contents
------------
 - 30 ft roll of McMaster-Carr fiberglass sheet insulation
 - 10 ft of 10" aluminum flashing
 - One complete control PCB
 - One STMicroelectronics Nucleo L031K6 board
 - One circuit breaker
 - Three relays
 - One thermocouple
 - One stepper motor and 3D printed bracket
 - One roll of 2" Kapton tape
 - 15 ft of 2" gold foil tape
 - Hardware to mount all components to the oven chassis
 - Assorted wiring and connectors

Make Your Own
-------------
Virtually all of the kit's components are off-the-shelf.  The only non-standard components are the oven door actuator and the controller PCB.  [Schematics are freely available for these components](https://gitlab.com/vtc-reflow-toaster/cad).

Required Tools
--------------
A number of common tools are required to install the kit:
 - Phillips head screwdriver
 - Tin snips
 - Drill, 4mm (~5/32") bit and countersink
 - Wire strippers and crimpers

Installation Procedure
----------------------
Specific instructions cannot be provided for all ovens.  Most ovens will require essentially the same conversion procedure.

1. Disassemble the oven.  The chassis must be removed, and the wiring of the oven will need to be disconnected or cut.  Some wiring may be welded to metal tabs, which cannot be easily soldered.  When cutting such wires, it is helpful to preserve as much of the wire as possible to avoid needing welding equipment.
2. Some ovens have heating elements on the top and bottom, with higher-powered elements on top for broiling.  If possible, swap the heating elements to minimize the incident infrared radiation reaching the top (populated) face of the PCBs being soldered.
3. Mount the relays to the interior of the oven chassis.  It may be necessary to drill holes to mount them.
4. Mount the control board to the exterior of the oven chassis.
5. Drill holes and mount the stepper motor bracket so the actuator arm reaches the corner of the oven door[^1].  Mount the stepper motor into the bracket and install the gears.
6. Drill and countersink a hole in the oven to allow the thermocouple to sense the interior temperature.
7. Measure the faces of the oven and cut insulation to fit.  Attach two or more layers on each face, attaching with Kapton tape as needed.
8. Cut, fold and mount aluminum flashing over the heating elements to reduce infrared heat transfer from the heating elements to the PCB being soldered[^2].  If possible, form ducts to channel air being circulated by the fan across the elements.
9. Reassemble the oven chassis.

[^1]: The existing actuator design needs improvements; see its README for details.
[^2]: In all likelihood, IR heat transfer is completely unavoidable; the effectiveness of simple aluminum shields has not yet been evaluated.  Complex ductwork and an aftermarket fan are probably necessary to make convection even remotely effective, but are beyond the scope of this project.
