Reflow Oven Software
====================
The Reflow Toaster is controlled exclusively through a desktop application.  For simplicity, communication between the application and the oven occurs through the "Virtual COM Port" built into the Nucleo board.  Drivers may be required, especially on Windows.

The software has only been tested on Arch Linux.  It should work on other Linux-based systems, as well as most UNIX-like systems.  The status of Windows support is unknown; no attempt has been made to compile or flash the software using Windows.

Requirements
------------
A modern Rust toolchain is required to compile the software.  The easiest way to get started is an installer from [rustup.rs](rustup.rs).  rustup is also available through most distributions' package managers.  Development tracks the latest stable version; Rust 1.34 was used at the time of writing.

OpenOCD and GDB are used to flash and debug the controller firmware.  They are available through most distributions' package managers.  OpenOCD version 0.10.0 has been tested, but older versions should also work.

Setting Up
----------
Fetch a copy of the source code: either [download a .zip of the latest source](https://gitlab.com/vtc-reflow-toaster/src/-/archive/master/src-master.zip) or clone the repository using git: `git clone https://gitlab.com/vtc-reflow-toaster/src.git vtc-reflow-oven`

Installing the Firmware
-----------------------
With the Nucleo board connected by USB:
```
cd vtc-reflow-oven/controller
cargo run --release
```
Note that the software must be built in release mode using `--release`; the debug binary is much too large to be flashed.

Compiling the UI
----------------
```
cd vtc-reflow-oven/hostui
cargo build --release
cp target/release/hostui /somewhere/on/your/PATH/reflow-ui
```
