Reflow Toaster
==============

Reflow soldering has many advantages, but requires specialized hardware.  For hobbyists, good reflow equipment is too expensive and to bulky to realistically use.  Adapting toaster ovens into reflow ovens is a popular project.  Pre-existing designs of varying price and quality are available.  This kit attempts to be much less expensive than others while still providing everything necessary to create a functional oven.

Although the most visible aspect of this project is its conversion of a toaster oven to a reflow oven, the most novel aspect is the software that powers the converted oven.  Rather than include a fiddly LCD screen and keypad or touchscreen, a desktop application is provided to provide rich control over the oven's operation.  All source code is available, and modifications are encouraged.

The project served a second purpose: to learn more about the Rust programming language and evaluate its usefulness in embedded software.  A wide range of tools and libraries are available to make embedded Rust development more productive, and the authors owe a debt of gratitude to the authors of these tools.
