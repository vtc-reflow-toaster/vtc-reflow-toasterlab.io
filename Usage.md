Using the Oven
==============

Connecting to the Oven
----------------------
The Reflow Toaster relies on a desktop application for configuration and operation.  A USB connection provides power for the controller and allows the desktop application to communicate with the controller.  To connect the desktop application to the oven:
1. Use a micro-USB cable to plug the board into a PC.
2. Start the desktop application or press the "Refresh" button in the upper-left to detect available devices.
3. Select a device in the drop-down menu in the upper-left, then click the "Connect" button.

Note that suspending the PC while the board is connected upsets the Nucleo board's debugger chip and breaks communication.  It may be necessary to unplug and reconnect the board in order to connect to the device after suspending the PC.

Aligning the Door Actuator
--------------------------
The "Jog Door" command on the left allows the home position of the door actuator to be changed.  Entering a number of counts and clicking the "Start" button in the upper-right will move the door that number of counts and reset the home position to the new position of the actuator.  Positive numbers move the actuator outward.

Configuring a Temperature Profile
---------------------------------
The default temperature profile should provide sensible defaults for ordinary solder paste, but may not be ideal for all pastes or all ovens.  It can be configured under the "Temperature Profile" button on the left.

Configuring the Temperature Controller
--------------------------------------
Currently, only one temperature control algorithm is available.  Its configuration is available under the "Bang-Bang Control" button on the left.  This controller is extremely simple and must be manually tuned through experimentation.  Several settings are available:
 - "Lookahead time": the control algorithm looks at the current temperature and how quickly the temperature is changing to predict how hot the oven will be in the future.  This setting controls how far ahead the controller will look.  Too large a value will excessively smooth out the temperature profile, while too little will cause excessive delays and overshoot.
 - "Element hysteresis": the control algorithm looks at the predicted temperature to determine when to turn the elements on or off.  The "on" and "off" values indicate temperature difference from the ideal temperature.  If the predicted temperature will be less than the ideal plus the "on" value, the element is switched on; if it is greater than thel ideal plus the "off" value, it is switched off.
 - "Door open position": when cooling the oven off, the control algorithm will completely open or close the door.  The open position is the number of stepper motor counts that is used for the open position.
 - "Door hysteresis": similar to the "element hysteresis" values, but applied to opening and closing the door during cooling.  In practice, these settings have no effect; the door must always be opened completely during cooling.

Operating the Oven
------------------
With the controller connected and the "Bang-Bang Control" button selected, click the "Start" button in the top-right.  This will send the currently-configured temperature profile and control parameters to the oven and instruct it to begin operation.  As it heats and cools, its temperature will be displayed alongside the actual temperature profile.
