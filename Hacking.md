Hacking
=======
The oven's software is intended to be hackable, often to the detriment of "good" implementation.

Configuration Data
------------------
Configuration is stored by the UI in mostly free-form JSON, with no particular requirements on structure.  A single instance of `hostui::state::State` holds configuration data and is written to disk whenever requested.  This instance is available through the `hostui::ui::Model` that is passed around.

Communication with Controller
-----------------------------
The communication protocol isn't explicitly defined.  Instead, serde is abused to give the appearance that Rust objects are sent directly over the wire.  `common::protocol::HostMsg` and `::CtrlMsg` `enum`s contain all possible messages originating from the host UI and controller, respectively.  If these `enum`s (or their contained types) are changed, both the UI and firmware will need to be recompiled to remain in sync.

The actual wire protocol consists of several layers.  First, Rust objects are flattened to bare bytes using a modified version of the ssmarshal library.  Second, a simple checksum byte is added after the message; the sum of all bytes in the message modulo 256 should equal 0.  Third, the message is transformed using Consistent Overhead Byte Stuffing (COBS) to remove all zero bytes.  Fourth, a zero byte is appended to indicate the end of the message.

The modifications to ssmarshal have two goals.  First, restrictions on serialized size are removed, allowing  `&str` and `&[u8]` to be serialized; this is used for transferring debug messages from the controller to the UI.  Second, support is added for generic byte sink objects, allowing the entire encoding process to occur without intermediate copies.

Adding New Operations
---------------------
Because of the flexible protocol, it is easy to wire new commands between the UI and the controller.  The source refers to these as `Operation`s; all operations are located in the `common` crate and implement trait `common::operation::AbstractOperation`.  `Operation` objects will be notified when they begin executing (`start()`), when they are stopped by the user (`abort()`), and once per second (`run()`).  The `Operation` has exclusive control over the oven until its `run()` method returns `Completed::Complete`.  These controls are exposed through a `common:operation::AbstractControls` reference passed to `run()`.

Each command has an associated page in the UI.  When the command is running, the UI forwards messages from the controller to the command's page `struct`.  Commands can relay data back to the user interface this way; a few simple reports are available through `common::protocol::CtrlMsg`, and others can be added.  Currently, there is no way to send generic reports from the controller, and no way to send data directly to a running command.

Although not yet implemented, a simulator is planned.  This would allow commands to be executed on a simulated oven and the results displayed.  This would be most useful for more complex control schemes.  No modifications to the existing commands would be required to support this.

Four main steps are required to add a new command:
1. Create a `struct` for the command in a module under `common::operation::` and `impl` trait `common::operation::AbstractOperation`.
2. Add the command to `enum operation::Operation` to allow it to be sent over the wire to the oven.
3. Create a user interface for the command under `hostui::ui::operation::`.
4. Add the interface to `hostui::ui::main::Win` and initialize it in `view()`.
